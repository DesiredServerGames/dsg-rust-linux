## Ansible playbook: Rust and Rust Oxide Mod

[README English version](./README.EN.md)

## Description
Installation, configure et sécurisation d'un serveur Rust avec ou sans Oxide Mod sur RHEL/Fedora, Debian/Ubuntu.

## Visuels
__Debian/Ubuntu__

_Youtube Bientôt disponible_

__RHEL/Fedora__

_Youtube Bientôt disponible_

## Installation
#### /!\ Mettre à jour au préalable les valeurs des variables se trouvant dans `group_vars/all.yml` avant de lancer la dernière commande.

Debian/Ubuntu Family
```bash
sudo apt-get -y update && sudo apt-get -y upgrade
sudo apt-get -y install ansible git
git clone https://gitlab.com/DesiredServerGames/dsg-rust-linux.git
cd dsg-valheim-linux
ansible-galaxy install -r requirements.yml
ansible-playbook -i localhost main.yml
```
RHEL/Fedora  Family
```bash
sudo yum -y update && sudo yum -y upgrade
sudo yum -y install ansible git
git clone https://gitlab.com/DesiredServerGames/dsg-rust-linux.git
cd dsg-valheim-linux
ansible-galaxy install -r requirements.yml
ansible-playbook -i localhost main.yml
```

## Role Variables

Changer les valeurs des ports selon votre convenance
```yaml
dsg_user_password: supertintin
steamcmd_login_timeout: 240
rust_port: 28015
rust_port_rcon: 28016
rust_rcon_password: superpassword
rust_server_name: monserveur
rust_identity: tintin
rust_password: abc123def4896
rust_public: 1
rust_install_timeout: 3600
rust_oxide_mod: true
rust_max_players: 24
rust_level: "Barren"
rust_seed: 12345
rust_worldsize: 3000
rust_saveinterval: 300
rust_description: mon server RolePlay
rust_headerimage: ~
rust_url_website: ~
```

### Dependances
- dsg-role-user
- dsg-role-firewall
- dsg-role-fail2ban
- dsg-role-steamcmd


### Example Playbook
```yaml
- hosts: localhost
  become: yes
  gather_facts: True

  roles:
    - { role: dsg-role-user }
    - { role: dsg-role-steamcmd }
    - { role: dsg-role-firewall }
    - { role: dsg-role-fail2ban }
    - { role: dsg-role-rust }
```
### Support
Utiliser les issues prévu à cette effet

### Roadmap
Pour l'instant rien

### License
MIT

### Author Information
Ce rôle a été créé en 2021 par Didier MINOTTE.

